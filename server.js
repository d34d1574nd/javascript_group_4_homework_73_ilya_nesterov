const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const work = express();

const port = 8000;

work.get('/', (req, res) => {
    res.send('HomePage')
});

work.get('/:name', (req, res) => {
    res.send(req.params.name)
});

work.get('/cipher/:name', (req, res) => {
    res.send(Vigenere.Cipher('password').crypt(req.params.name))
});

work.get('/decipher/:name', (req, res) => {
    res.send(Vigenere.Decipher('password').crypt(req.params.name))
});

work.listen(port, () => {
    console.log(port)
});